--
-- This script needs to run only once
--
DROP DATABASE IF EXISTS JSF_PASSWORD;
CREATE DATABASE JSF_PASSWORD;

-- Best practice MySQL as of 5.7.6
DROP USER IF EXISTS user@localhost;
CREATE USER user@'localhost' IDENTIFIED WITH mysql_native_password BY 'pencil' REQUIRE NONE;
GRANT ALL ON JSF_PASSWORD.* TO user@'localhost';

-- Best practice MySQL prior to 5.7.6
-- GRANT ALL PRIVILEGES ON JSF_PASSWORD.* TO user@'localhost' IDENTIFIED BY 'pencil';

FLUSH PRIVILEGES;
