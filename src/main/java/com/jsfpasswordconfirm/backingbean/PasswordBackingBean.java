package com.jsfpasswordconfirm.backingbean;

import com.jsfpasswordconfirm.controller.UserTableJpaController;
import com.jsfpasswordconfirm.entities.User;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Backing bean for index.xhtml
 *
 * @author Ken Fogel
 */
@Named("passwordBacking")
@SessionScoped
public class PasswordBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(PasswordBackingBean.class);

    @Inject
    private UserTableJpaController controller;

    // The entity object
    private User user;
    // The value for the confirmPassword field that does not exist in the entity
    private String passwordConfirm;

    /**
     * User created if it does not exist.
     *
     * @return
     */
    public User getUser() {
        LOG.debug("getUser");
        if (user == null) {
            LOG.debug("Creating new User entity object");
            user = new User();
        }
        LOG.debug("User: " + user.hashCode() + "   " + user.toString());
        return user;
    }

    /**
     * Save the current person to the db
     *
     * @return
     * @throws Exception
     */
    public String createUser() throws Exception {
        LOG.debug("Writing User entity to db: " + user.toString());
        controller.create(user);
        return "doSomething";
    }

    /**
     * Getter for the password confirm field
     *
     * @return
     */
    public String getPasswordConfirm() {
        LOG.debug("getPasswordConfirm");
        return passwordConfirm;
    }

    /**
     * Setter for the password confirm field
     *
     * @param passwordConfirm
     */
    public void setPasswordConfirm(String passwordConfirm) {
        LOG.debug("setPasswordConfirm");
        this.passwordConfirm = passwordConfirm;
    }

    /**
     * The method that compares the two password fields correctly
     *
     * @param context
     * @param component
     * @param value
     */
    public void validatePasswordCorrect(FacesContext context, UIComponent component,
            Object value) {

        LOG.debug("validatePasswordCorrect");

        // Retrieve the value passed to this method
        String confirmPassword = (String) value;

        // Retrieve the temporary value from the password field
        UIInput passwordInput = (UIInput) component.findComponent("password");
        //String password = (String) passwordInput.getLocalValue();

        if (user.getPassword() == null || confirmPassword == null || !user.getPassword().equals(confirmPassword)) {
            LOG.debug("validatePasswordCorrect: " + user.getPassword() + " and " + confirmPassword);
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['nomatch']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            throw new ValidatorException(msg);
        }
    }

    /**
     * The method verifies that the Login name is not already in the database
     *
     * @param context
     * @param component
     * @param value
     */
    public void validateUniqueUser(FacesContext context, UIComponent component,
            Object value) {

        LOG.debug("validateUniquePassword");

        // Retrieve the value passed to this method
        String loginName = (String) value;

        LOG.debug("validateUniquePassword: " + loginName);
        if (controller.findUser(loginName) != null) {
            String message = context.getApplication().evaluateExpressionGet(context, "#{msgs['duplicate']}", String.class);
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message);
            throw new ValidatorException(msg);
        }
    }

}
