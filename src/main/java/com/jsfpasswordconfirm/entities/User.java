package com.jsfpasswordconfirm.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generated Entity Class
 * 
 * @author Ken Fogel
 */
@Entity
@Table(name = "user_table", catalog = "JSF_PASSWORD", schema = "")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "LOGIN_NAME")
    private String loginName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "PASSWORD")
    private String password;

//    private final static Logger LOG = LoggerFactory.getLogger(User.class);
    
    public User() {
    }

    public User(String loginName) {
        this.loginName = loginName;
    }

    public User(String loginName, String password) {
        this.loginName = loginName;
        this.password = password;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
//        LOG.debug("getPassword in User: " + password);
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
//        LOG.debug("setPassword in User: " + password);
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (loginName != null ? loginName.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof User)) {
//            return false;
//        }
//        User other = (User) object;
//        if ((this.loginName == null && other.loginName != null) || (this.loginName != null && !this.loginName.equals(other.loginName))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return "com.jsfpasswordconfirm.entities.UserTable[ loginName=" + loginName + " password=" + password + "]";
    }
    
}
