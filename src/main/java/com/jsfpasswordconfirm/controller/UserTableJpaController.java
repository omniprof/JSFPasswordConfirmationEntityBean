package com.jsfpasswordconfirm.controller;

import com.jsfpasswordconfirm.controller.exceptions.RollbackFailureException;
import com.jsfpasswordconfirm.entities.User;
import java.io.Serializable;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

/**
 * Generated JPA Controller trimmed to just the create method
 *
 * @author Ken Fogel
 */
@Named
@RequestScoped
public class UserTableJpaController implements Serializable {
    
    @Resource
    private UserTransaction utx;

    @PersistenceContext(unitName = "JSFPasswordConfirmPU")
    private EntityManager em;

    public void create(User user) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            em.persist(user);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }
    
    public User findUser(String loginName) {
        return em.find(User.class, loginName);
    }
    
}
